import java.util.Scanner;
class  operations
{

	float add(float a, float b)
	{
		return a+b;
	}

	float subtract(float a, float b)
	{
		return a-b;
	}

	float multiply(float a, float b)
	{
		return a*b;
	}

	float divide(float a, float b)
	{
		return a/b;
	}
}
public class test
{
	public static void main(String args[])
	{
		operations obj = new operations();
		final float arr[] = new float[4];
		final float a = Float.parseFloat(args[0]);
		final float b = Float.parseFloat(args[1]);
		final int choice = Integer.parseInt(args[2])-1;
		arr[0] = obj.add(a,b);
		arr[1] = obj.subtract(a,b);
		arr[2] = obj.multiply(a,b);
		arr[3] = obj.divide(a,b);

		System.out.println("Answer: "+arr[choice]);


	}
}